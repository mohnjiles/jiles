﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JilesMediaPlayer.Models
{
    public class Song : Media
    {
        public string Artist { get; set; }
        public string Album { get; set; }
        public string Length { get; set; }
        //public int PlayCount { get; set; }
        public int Year { get; set; }
        public string Genre { get; set; }
        public double TotalSeconds { get; set; }
    }
}
