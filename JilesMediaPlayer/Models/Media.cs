﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JilesMediaPlayer.Models
{
    public class Media
    {
        public string Title { get; set; }
        public string FileLocation { get; set; }
    }
}
