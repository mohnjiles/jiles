﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Threading;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Threading;
using JilesMediaPlayer.Models;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Newtonsoft.Json;
using TagLib;
using Application = System.Windows.Application;
using File = TagLib.File;

namespace JilesMediaPlayer.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private string _lastSelectedPath;
        private readonly MediaPlayer _mediaPlayer;
        private readonly DispatcherTimer _seekTimer;
        private bool _isDragging = false;

        #region Construtor

        public MainViewModel()
        {
            MusicDirectories = LoadMusicDirectories();
            AddMusicDirectory = new Command(async () => await DoAddMusicDirectory());
            WindowLoadedCommand = new Command(async () => await WindowLoaded());
            NextSongCommand = new Command(SkipToNextSong);
            PreviousSongCommand = new Command(SkipToPreviousSong);
            DataGridSortingCommand = new Command(OnSort);

            _mediaPlayer = new MediaPlayer();
            _mediaPlayer.MediaEnded += (sender, args) =>
            {
                SkipToNextSong();
                IsPaused = false;
            };


            _seekTimer = new DispatcherTimer
            {
                Interval = TimeSpan.FromMilliseconds(200)
            };
            _seekTimer.Tick += _seekTimer_Tick;

            IsPaused = true;
        }

        #endregion Constructor

        public event PropertyChangedEventHandler PropertyChanged;

        #region Event Handlers

        private async Task WindowLoaded()
        {
            if (ConfigurationManager.AppSettings["MusicDirectories"] == null)
                await DoAddMusicDirectory();
            else
                await LoadMusicFiles();
        }

        public void OnMouseDoubleClick(int index)
        {
            var song = (Song) SongListView.GetItemAt(index);
            _currentSongIndex = index;
            _mediaPlayer.Open(new Uri(song.FileLocation));
            _mediaPlayer.Play();

            CurrentSongLength = (int)song.TotalSeconds;

            IsPaused = false;
        }

        private void OnPause()
        {
            if (!_mediaPlayer.CanPause) return;

            _mediaPlayer.Pause();
            IsPaused = true;
            _seekTimer.Stop();
        }

        private void OnPlay()
        {
            IsPaused = false;
            _seekTimer.Start();
            _mediaPlayer.Play();
        }

        private void OnSort()
        {
            SongListView.Refresh();
        }

        public void OnDragStarted()
        {
            _isDragging = true;
        }

        public void OnDragCompleted(double value)
        {
            _isDragging = false;
            _mediaPlayer.Position = TimeSpan.FromSeconds(value);
        }

        public void OnPropertyChanged(string name)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(name));
        }

        public void SkipToNextSong()
        {
            Song nextSong;
            try
            {
                nextSong = (Song) SongListView.GetItemAt(++CurrentSongIndex);
            }
            catch (ArgumentOutOfRangeException)
            {
                IsPaused = true;
                return;
            }

            if (nextSong == null) return;

            CurrentSongLength = (int)nextSong.TotalSeconds;

            _mediaPlayer.Stop();
            _mediaPlayer.Open(new Uri(nextSong.FileLocation));
            _mediaPlayer.Play();
        }

        public void SkipToPreviousSong()
        {
            Song previousSong;
            try
            {
                previousSong = (Song) SongListView.GetItemAt(--CurrentSongIndex);
            }
            catch (ArgumentOutOfRangeException)
            {
                IsPaused = true;
                return;
            }

            if (previousSong == null) return;

            CurrentSongLength = (int)previousSong.TotalSeconds;

            _mediaPlayer.Stop();
            _mediaPlayer.Open(new Uri(previousSong.FileLocation));
            _mediaPlayer.Play();
        }

        #endregion


        private async Task DoAddMusicDirectory()
        {
            var dialog = new FolderBrowserDialog { SelectedPath = _lastSelectedPath, Description = "Select Music Folder" };
            var result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                MusicDirectories.Add(dialog.SelectedPath);
                _lastSelectedPath = dialog.SelectedPath;
                SaveMusicDirectories(MusicDirectories);
                await LoadMusicFiles();
            }
        }

        private void SaveMusicDirectories(ObservableCollection<string> musicDirectories)
        {
            var json = JsonConvert.SerializeObject(musicDirectories);
            AddUpdateAppSettings("MusicDirectories", json);
        }

        private static ObservableCollection<string> LoadMusicDirectories()
        {
            var collection = new ObservableCollection<string>();
            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                var json = appSettings["MusicDirectories"];
                if (json != null)
                    collection = JsonConvert.DeserializeObject<ObservableCollection<string>>(json);
            }
            catch (ConfigurationErrorsException ex)
            {
                Console.WriteLine("Error reading app settings: {0} ", ex);
            }

            return collection;
        }

        private async Task LoadMusicFiles()
        {
            var metroWindow = (Application.Current.MainWindow as MetroWindow);
            var progress = await metroWindow.ShowProgressAsync("Please wait...", "Loading music...");
            progress.SetIndeterminate();

            await Task.Run(() =>
            {
                foreach (var directory in MusicDirectories)
                {
                    var files = Directory.EnumerateFiles(directory, "*.*", SearchOption.AllDirectories)
                                        .Where(file => file.ToLower().EndsWith("mp3") || file.ToLower().EndsWith("m4a"));

                    foreach (var file in files)
                    {
                        File musicFile = null;
                        try
                        {
                            musicFile = File.Create(file);
                        }
                        catch (UnsupportedFormatException)
                        {
                        }

                        if (musicFile != null)
                        {
                            var duration = musicFile.Properties.Duration;
                            //var hours = duration.Hours < 1 ? "" : $"{duration.Hours.ToString("00")}:";
                            var minutes = duration.TotalMinutes < 1 ? "" : $"{duration.TotalMinutes.ToString("00")}:";
                            var seconds = $"{duration.Seconds.ToString("00")}";
                            var durationFormatted = $"{minutes}{seconds}";


                            DispatcherHelper.CheckBeginInvokeOnUI(() =>
                            {
                                SongList.Add(new Song
                                {
                                    Artist = string.IsNullOrEmpty(musicFile.Tag.AlbumArtists.FirstOrDefault())
                                        ? musicFile.Tag.Performers.FirstOrDefault()
                                        : musicFile.Tag.AlbumArtists.FirstOrDefault(),
                                    Title = musicFile.Tag.Title,
                                    Length = durationFormatted,
                                    FileLocation = file,
                                    TotalSeconds = duration.TotalSeconds,
                                    Album = musicFile.Tag.Album
                                });
                            });
                        }
                    }

                    DispatcherHelper.CheckBeginInvokeOnUI(async () => { if (progress.IsOpen) await progress.CloseAsync(); });
                }
            });
        }

        private static void AddUpdateAppSettings(string key, string value)
        {
            try
            {
                var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = configFile.AppSettings.Settings;
                if (settings[key] == null)
                {
                    settings.Add(key, value);
                }
                else
                {
                    settings[key].Value = value;
                }
                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            }
            catch (ConfigurationErrorsException)
            {
                Console.WriteLine(@"Error writing app settings");
            }
        }

        private void _seekTimer_Tick(object sender, EventArgs e)
        {
            if (!_isDragging)
            {
                CurrentSongPosition = (int)_mediaPlayer.Position.TotalSeconds;
            }
        }

        #region Properties

        private ObservableCollection<string> _musicDirectories;

        public ObservableCollection<string> MusicDirectories
        {
            get { return _musicDirectories ?? (_musicDirectories = new ObservableCollection<string>()); }
            set
            {
                _musicDirectories = value;
                OnPropertyChanged("MusicDirectories");
            }
        }

        private ObservableCollection<Song> _songList;

        public ObservableCollection<Song> SongList
        {
            get { return _songList ?? (_songList = new ObservableCollection<Song>()); }
            set
            {
                _songList = value;
                OnPropertyChanged("SongList");
                SongListView.Refresh();
            }
        }

        private ListCollectionView _songListView;

        public ListCollectionView SongListView
        {
            get { return _songListView ?? (_songListView = new ListCollectionView(SongList)); }
            set
            {
                _songListView = value;
                OnPropertyChanged("SongListView");
            }
        }

        private bool _isPaused;
        public bool IsPaused
        {
            get { return _isPaused; }
            set
            {
                if (_isPaused == value) return;
                _isPaused = value;
                OnPropertyChanged("IsPaused");

                if (value)
                    OnPause();
                else OnPlay();
            }
        }

        private int _currentSongLength;
        public int CurrentSongLength
        {
            get { return _currentSongLength; }
            set
            {
                if (_currentSongLength == value) return;

                _currentSongLength = value;
                OnPropertyChanged("CurrentSongLength");

            }
        }

        private int _currentSongPosition;
        public int CurrentSongPosition
        {
            get { return _currentSongPosition; }
            set
            {
                if (_currentSongPosition == value) return;

                _currentSongPosition = value;
                OnPropertyChanged("CurrentSongPosition");
            }
        }

        private int _currentSongIndex;

        public int CurrentSongIndex
        {
            get { return _currentSongIndex; }
            set
            {
                if (_currentSongIndex == value) return;

                _currentSongIndex = value;
                OnPropertyChanged("CurrentSongIndex");
                Messenger.Default.Send<int>(_currentSongIndex);
            }
        }

        public Command AddMusicDirectory { get; private set; }
        public Command RemoveDirectory { get; private set; }
        public Command WindowLoadedCommand { get; private set; }
        public Command NextSongCommand { get; private set; }
        public Command PreviousSongCommand { get; private set; }
        public Command DataGridSortingCommand { get; private set; }

        #endregion
    }
}