﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Interop;
using GalaSoft.MvvmLight.Messaging;
using JilesMediaPlayer.Models;
using JilesMediaPlayer.ViewModels;
using MahApps.Metro.Controls;
using DataGrid = System.Windows.Controls.DataGrid;

namespace JilesMediaPlayer.Views
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private const int PlayPauseHotkey = 9000;
        private const int NextTrackHotkey = 9001;
        private const int PreviousTrackHotkey = 9002;

        public MainWindow()
        {
            InitializeComponent();
            MainViewModel = new MainViewModel();
            DataContext = MainViewModel;
            Messenger.Default.Register<int>(this, UpdateSelectedItem);
        }

        public MainViewModel MainViewModel{ get; set; }

        #region Event Handlers

        private void DgMusics_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var grid = sender as DataGrid;
            MainViewModel.OnMouseDoubleClick(grid.SelectedIndex);
        }

        private void Thumb_OnDragStarted(object sender, DragStartedEventArgs e)
        {
            MainViewModel.OnDragStarted();
        }

        private void Thumb_OnDragCompleted(object sender, DragCompletedEventArgs e)
        {
            MainViewModel.OnDragCompleted(((Slider)sender).Value);
        }

        private void UpdateSelectedItem(int index)
        {
            if (index < 0 || index > dgMusics.Items.Count - 1) return;
            dgMusics.SelectedItem = dgMusics.Items[index];
        }

        #endregion


        #region Hotkey stuff

        [DllImport("User32.dll")]
        private static extern bool RegisterHotKey(
            [In] IntPtr hWnd,
            [In] int id,
            [In] uint fsModifiers,
            [In] uint vk);

        [DllImport("User32.dll")]
        private static extern bool UnregisterHotKey(
            [In] IntPtr hWnd,
            [In] int id);

        private HwndSource _source;
        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            var helper = new WindowInteropHelper(this);
            _source = HwndSource.FromHwnd(helper.Handle);
            _source.AddHook(HwndHook);
            RegisterHotKey(0x77, PlayPauseHotkey);
        }

        protected override void OnClosed(EventArgs e)
        {
            _source.RemoveHook(HwndHook);
            _source = null;
            UnregisterHotKey(PlayPauseHotkey);
            base.OnClosed(e);
        }

        private void RegisterHotKey(uint vkKeyCode, int hotkeyId)
        {
            var helper = new WindowInteropHelper(this);
            const uint ctrlMod = 0x0002;
            if (!RegisterHotKey(helper.Handle, hotkeyId, ctrlMod, vkKeyCode))
            {
                // handle error
            }
        }

        private void UnregisterHotKey(int hotkeyId)
        {
            var helper = new WindowInteropHelper(this);
            UnregisterHotKey(helper.Handle, hotkeyId);
        }

        private IntPtr HwndHook(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            const int WM_HOTKEY = 0x0312;
            const int WM_APPCOMMAND = 0x0319;
            switch (msg)
            {
                case WM_HOTKEY:
                    switch (wParam.ToInt32())
                    {
                        case PlayPauseHotkey:
                            MainViewModel.IsPaused = !MainViewModel.IsPaused;
                            handled = true;
                            break;
                    }
                    break;
                case WM_APPCOMMAND:

                    var cmd = (int)((uint)lParam >> 16 & ~0xf000);
                    switch (cmd)
                    {
                        case 13:  // APPCOMMAND_MEDIA_STOP constant
                            break;
                        case 14:  // APPCOMMAND_MEDIA_PLAY_PAUSE
                            MainViewModel.IsPaused = !MainViewModel.IsPaused;
                            handled = true;
                            break;
                        case 11:  // APPCOMMAND_MEDIA_NEXTTRACK
                            MainViewModel.SkipToNextSong();
                            handled = true;
                            break;
                        case 12:  // APPCOMMAND_MEDIA_PREVIOUSTRACK
                            MainViewModel.SkipToPreviousSong();
                            handled = true;
                            break;
                    }
                    break;
            }
            return IntPtr.Zero;
        }

        #endregion
    }
}